import argparse
import os
import shutil
import json
import sys

BASE_SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
RENDER_TEST_DIR = os.path.join(BASE_SCRIPT_PATH, "render_test")
if not os.path.exists(RENDER_TEST_DIR):
    print "Directory \"" + RENDER_TEST_DIR + "\" doesn't exist. Exit the program !"
    exit(-1)

sys.path.append(RENDER_TEST_DIR)
from test_utils import *

DESCRIPTIONS_DIR = os.path.join(RENDER_TEST_DIR, "descriptions")


class VertigoInstance:
    vcore = None

    def import_vertigo_python(self):
        # os.environ["PATH"] += ";" + self.vertigo_path # useless
        os.chdir(self.vertigo_path)
        sys.path.append(self.vertigo_path)
        sys.path.append(os.path.join(self.vertigo_path, "python-packages"))
        import vertigo.core
        self.vcore = vertigo.core
        print_info("Vertigo lib imported")

    def load_plugin(self, plugin):
        self.vcore.Document.loadPlugin(plugin)
        print_info("Plugin \"" + plugin + "\" was loaded")

    def relocate_json(self, json, textures_dir, proxy_texture_dir):
        textures_dict = {}
        for t, shdTree in json['ShadingTrees'].items():
            for n, node in shdTree['nodes'].items():
                for p, param in node['parameters'].items():
                    if param['type'] == 'file':
                        src_filenames = param['value']

                        if (isinstance(src_filenames, list)):
                            param["value"] = []
                            for f in src_filenames:
                                dst_filename = os.path.abspath(
                                    os.path.join(textures_dir, os.path.basename(f)))
                                if not dst_filename.endswith(".mem"):
                                    textures_dict[f] = dst_filename
                                    param["value"].append(os.path.join(
                                        proxy_texture_dir, os.path.basename(f)).replace('\\', '/'))
                        else:
                            dst_filename = os.path.abspath(os.path.join(
                                textures_dir, os.path.basename(src_filenames)))
                            if not dst_filename.endswith(".mem"):
                                textures_dict[src_filenames] = dst_filename
                                param["value"] = os.path.join(
                                    proxy_texture_dir, os.path.basename(src_filenames)).replace('\\', '/')
        return json, textures_dict

    def deprecated_relocate_json(self, json, textures_dir, proxy_texture_dir):
        textures_dict = {}
        for m in json['Materials']:
            for s in m['shaders']:
                if 'parameters' in s:
                    for p in s['parameters']:
                        if p['type'] == 'file':
                            src_filename = p['value']
                            dst_filename = os.path.abspath(os.path.join(
                                textures_dir, os.path.basename(src_filename)))
                            if not dst_filename.endswith(".mem"):
                                textures_dict[src_filename] = dst_filename
                                p['value'] = os.path.join(proxy_texture_dir, os.path.basename(
                                    src_filename)).replace('\\', '/')

        return json, textures_dict

    def is_udim(self, filename):
        return filename.find("<UDIM") != -1

    def list_udim(self, src_filename, dst_filename):
        udims = []
        src_split = os.path.basename(src_filename).split("<UDIM")
        src_pre_udim = src_split[0]
        src_post_udim = src_split[1] if src_split[1].find(
            ">") == -1 else src_split[1].split(">")[1]

        dst_split = os.path.basename(dst_filename).split("<UDIM")
        dst_pre_udim = dst_split[0]
        dst_post_udim = dst_split[1] if dst_split[1].find(
            ">") == -1 else dst_split[1].split(">")[1]

        src_dir = os.path.dirname(src_filename)
        src_files = os.listdir(src_dir)
        for f in src_files:
            if f.find(dst_pre_udim) != -1 and f.find(dst_post_udim) != -1:
                udim_id = f.split(dst_pre_udim)[1].split(dst_post_udim)[0]
                src_udim = os.path.join(os.path.dirname(
                    src_filename), src_pre_udim + udim_id + src_post_udim)
                dst_udim = os.path.join(os.path.dirname(
                    dst_filename), dst_pre_udim + udim_id + dst_post_udim)
                udims.append({"src": src_udim, "dst": dst_udim})
        return udims

    def relocate_textures(self, json_file, output_base):
        fd = open(json_file)
        j = json.load(fd)
        fd.close()

        textures_dir = os.path.join(output_base, TEXTURES_DIRECTORY)
        safe_mkdirs(textures_dir)
        proxy_texture_dir = os.path.join(PROXY_BASE_DIR, TEXTURES_DIRECTORY)

        try:
            src_to_dst = {}
            if "Materials" in j:
                j, src_to_dst = self.deprecated_relocate_json(
                    j, textures_dir, proxy_texture_dir)

            if "ShadingTrees" in j:
                j, src_to_dst = self.relocate_json(
                    j, textures_dir, proxy_texture_dir)
        except KeyError as e:
            print_error("Wrong json format for output file: " + json_file +
                        " (Missing key: " + str(e) + "). Some textures may not have been copied.")

        # copy
        if self.texture_copy:
            for src, dst in src_to_dst.iteritems():
                textures_to_copy = [{"src": src, "dst": dst}]
                if (self.is_udim(dst)):
                    textures_to_copy = self.list_udim(src, dst)
                for tex in textures_to_copy:
                    try:
                        shutil.copyfile(tex["src"], tex["dst"])
                    except IOError as e:
                        print_warning(
                            "Unable to find textures to relocate: " + tex["src"])

        fd = open(json_file, "w")
        fd.write(json.dumps(j, indent=4))

    def bake(self, output_tag, category, group, test, settings):
        # get useful paths
        vdb = str(os.path.join(self.workspace, settings["vdb_path"]))
        output_dir = os.path.join(
            self.output_path, output_tag, category, group)

        # get scene data
        graph_name = str(settings["graph"])
        output_file = str(os.path.join(
            output_dir, test) if not self.local_then_copy else os.path.join(BASE_SCRIPT_PATH, test))
        fr = FrameRange(settings)

        # create output directories
        safe_mkdirs(output_dir)

        try:
            # load vdb & graph
            self.doc.load(vdb)
            print_info("  |-> Vdb file opened: " + vdb)

            graph = self.vcore.Id(graph_name)
            print_info("  |-> Graph loaded: " + graph_name)

            # create SRN if it didn't exist
            srn = self.mm.macro("3D.Render.GetRenderNode").run(
                graph, "ShiningRenderNode", "camera", True)
            if not srn.isNodeValid():
                self.mm.macro("Shining.Create").run(graph)
                print_info(
                    "  |-> ShiningRenderNode created because it didn't exist")
                srn = self.mm.macro("3D.Render.GetRenderNode").run(
                    graph, "ShiningRenderNode", "camera", True)
                self.doc.setBypass(srn, True)

            print_info("  |-> " + fr.print_lite())

            # Call bioc and json macro
            self.mm.macro("Shining.IO.Bake3DSceneJson").run(
                graph, fr.start, fr.end, output_file)
            print_info("  |-> Output json file: " + output_file + ".json")

            self.mm.macro("Shining.IO.Bake3DSceneBioc").run(
                graph, fr.start, fr.end, output_file)
            print_info("  |-> Output json file: " + output_file + ".bioc")

            if self.local_then_copy:
                dst_file = os.path.join(output_dir, test)
                shutil.move(output_file + ".bioc", dst_file + ".bioc")
                shutil.move(output_file + ".json", dst_file + ".json")
                print_info("  |-> Output files copied to: " + output_dir)
                output_file = dst_file
        except RuntimeError as e:
            print_error("Vertigo failed during the bake: " + str(e))
            return

        # Relocate and copy textures
        self.relocate_textures(output_file + ".json", self.output_path)
        print_info("  |-> Texture relocated")

    def __init__(self, vertigo_path, workspace, output_path, texture_copy, local_then_copy):
        self.vertigo_path = os.path.abspath(vertigo_path)
        self.workspace = os.path.abspath(workspace)
        self.output_path = os.path.abspath(output_path)
        self.texture_copy = texture_copy
        self.local_then_copy = local_then_copy

        if self.vcore is None:
            self.import_vertigo_python()

        plugins = ["Macro", "3dNodes", "RPCShining"]
        for plugin in plugins:
            self.load_plugin(plugin)

        settingsProjectPath = os.path.join(vertigo_path, "bake_test_settings.vproj")
        self.vcore.SettingsProject.writeDefaultProject(settingsProjectPath)
        self.vcore.SettingsProject.setCurrent(settingsProjectPath)

        self.vcore.Document.create("vdb_baker")
        self.vcore.Document.setCurrent("vdb_baker")
        self.doc = self.vcore.Document.current()
        print_info("Document was created")

        self.mm = self.vcore.MacroManager.instance()

        self.vcore.initialiseGL()

        try:
            # set workspace
            self.doc.setProjectDirectory(self.workspace)
            print_info("Workspace set to: " + self.workspace)
        except RuntimeError as e:
            print_error("Vertigo failed while setting the workspace: " +
                        self.workspace + " (Message Error: " + str(e) + ")")
            exit(-1)

    def __del__(self):
        self.doc.clearDocument()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate usable scenes for Shining rendering test (bioc, json and linked media)")
    parser.add_argument("-v", "--vertigo", type=str, nargs="?", required=True,
                        help="Path to the Vertigo version to use for baking")
    parser.add_argument("-w", "--workspace", type=str, nargs="?", required=True,
                        help="Workspace where all the vdb and scene media are")
    parser.add_argument("-o", "--output", type=str, nargs="?", required=True,
                        help="Main output directory for the bioc/json and texture files")
    parser.add_argument("-t", "--tag", type=str, nargs="?",
                        help="Subdirectory name where output dataset will be generated. It's usefull if you want to generate datasets with different vertigo version. If not specified, dataset will be output to \"" + DEFAULT_OUTPUT_TAG + "\" directory")
    parser.add_argument("-f", "--filter", type=str, nargs="?", help="Filter specific tests, category or group to generate only these tests. Pattern : \"category:group:test\". You don't have to specify each filter tier (e.g. you can write \"category:group\"). You can exclude a tier by using '~' (e.g. \"category:~group\" will select every tests in category, except those in the group group)")
    parser.add_argument("--local_then_copy", action="store_true",
                        help="If set, the bioc/json files will be generated near the script before being copied in the ouput directory. Useful if your output directory is a network drive.")
    parser.add_argument("--no_texture_copy", action="store_true",
                        help="If set, the textures pointed in the generated json files won't be copied in the output directory")
    args = parser.parse_args()

    tests_start_time = get_current_time()

    # Setup a VertigoInstance
    v_inst = VertigoInstance(args.vertigo, args.workspace,
                             args.output, not args.no_texture_copy, args.local_then_copy)

    # Get default bake settings
    default_settings = load_json_file(os.path.join(
        DESCRIPTIONS_DIR, DEFAULT_TEST_SETTINGS), "quit")

    # Scanning test_set dir to list bakes to do
    test_descs = scanTestDescriptionsFromJsonFiles(
        DESCRIPTIONS_DIR, default_settings, args.filter)
    test_count = len(test_descs)

    print_info("[ {} tests listed ]\n".format(test_count))

    # Iter on tests
    count_token = 0
    for desc in test_descs:
        count_token += 1
        category = desc["category"]
        group = desc["group"]
        test = desc["test"]
        try:
            print_info("-- [{:d}/{:d}] Start test {:s}:{:s}:{:s}".format(
                count_token, test_count, category, group, test))
            v_inst.bake(args.tag if args.tag is not None else DEFAULT_OUTPUT_TAG,
                        category, group, test, desc["settings"])
            print ""
        except KeyError as e:
            print_error("Wrong format for test \"{:s}:{:s}:{:s}\" (Missing key: {:s})\n".format(
                category, group, test, str(e)))

    print_info("Bake finished. Total duration : " +
               pretty_str_time(duration_from(tests_start_time)))
